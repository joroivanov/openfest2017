import io.restassured.RestAssured;
import io.restassured.config.LogConfig;
import io.restassured.filter.log.LogDetail;
import io.restassured.response.Response;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.startsWith;

public class Tests extends TestFixture {

    /**
     * Write test case that verifies that invoking:
     *
     * /comments
     * /albums
     * /todos
     *
     * returns status code 200
      */
    @Test
    public void verifyStatusCode() {

    }

    /**
     * Write test case that verifies that the response of
     * /posts
     * matches the posts-json-schema
     */
    @Test
    public void jsonSchemaTest() {

    }

    /**
     * Write test case that verifies the response body of single comment
     */
    @Test
    public void checkSinglePostResponseTest() {

    }


    /**
     * Write test case that verifies the response of a single comment specified with named parameter
     */
    @Test
    public void commentTestWithNamedParameter() {

    }

    /**
     * Write test case that verifies the response of a single comment specified with unnamed parameter
     */
    @Test
    public void commentTestWithUnnamedParameter() {

    }

    /**
     * Write test case that verifies that the zipcodes of all users living in City ending with 'gh' are "92998-3874", "90566-7771"
     */
    @Test
    public void usersWithAptTest() {

    }
}
