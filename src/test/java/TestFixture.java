import io.restassured.RestAssured;
import org.junit.Before;

public class TestFixture {
    public String posts;
    public String users;
    public String albums;
    public String photos;
    public String todos;
    public String comments;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 3000;
        posts = "/posts";
        users = "/users";
        albums = "/album";
        photos = "/photos";
        todos = "/todos";
        comments = "/comments";
    }
}
