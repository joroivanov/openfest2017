import io.restassured.config.LogConfig;
import io.restassured.filter.log.LogDetail;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.*;
import static org.hamcrest.Matchers.*;

import static io.restassured.path.json.JsonPath.from;

public class RestAssuredTests extends TestFixture {
    @Test
    public void getAllPosts() {
        when()
                .get(posts)
                .then()
                .statusCode(200);
    }


    @Test
    public void jsonSchemaTest() {
        File jsonSchema = new File(System.getProperty("user.dir") + "/src/test/resources/users-json-schema.json");

        when()
                .get(users)
                .then()
                .body(matchesJsonSchema(jsonSchema));
    }


    @Test
    public void checkSinglePostResponseTest() {
        when()
                .get(posts + "/2")
                .then()
                .statusCode(200)
                .body("userId", equalTo(1))
                .and().body("id", equalTo(2))
                .and().body("title", equalTo("qui est esse"))
                .body("body", startsWith("est rerum tempore vitae\nsequi sint nihil reprehenderit dolor"));
    }


    @Test
    public void userTest() {
        when()
                .get(users + "/3")
                .then()
                .body("id", equalTo(3))
                .body("name", equalTo("Clementine Bauch"))
                .body("username", equalTo("Samantha"))
                .body("email", equalTo("Nathan@yesenia.net"))
                .body("address.street", equalTo("Douglas Extension"))
                .body("address.suite", equalTo("Suite 847"))
                .body("address.city", equalTo("McKenziehaven"))
                .body("address.zipcode", equalTo("59590-4157"))
                .body("address.geo.lat", equalTo("-68.6102"))
                .body("address.geo.lng", equalTo("-47.0653"))
                .body("phone", equalTo("1-463-123-4447"))
                .body("website", equalTo("ramiro.info"))
                .body("company.name", equalTo("Romaguera-Jacobson"))
                .body("company.catchPhrase", equalTo("Face to face bifurcated interface"))
                .body("company.bs", equalTo("e-enable strategic applications"))
                .statusCode(200);
    }


    @Test
    public void userTestWithNamedParameter() {
        int userId = 3;
        given()
                .pathParam("userId", userId)
                .when()
                .get(users + "/{userId}")
                .then()
                .body("id", equalTo(userId))
                .body("name", equalTo("Clementine Bauch"))
                .body("username", equalTo("Samantha"))
                .body("email", equalTo("Nathan@yesenia.net"))
                .body("address.street", equalTo("Douglas Extension"))
                .body("address.suite", equalTo("Suite 847"))
                .body("address.city", equalTo("McKenziehaven"))
                .body("address.zipcode", equalTo("59590-4157"))
                .body("address.geo.lat", equalTo("-68.6102"))
                .body("address.geo.lng", equalTo("-47.0653"))
                .body("phone", equalTo("1-463-123-4447"))
                .body("website", equalTo("ramiro.info"))
                .body("company.name", equalTo("Romaguera-Jacobson"))
                .body("company.catchPhrase", equalTo("Face to face bifurcated interface"))
                .body("company.bs", equalTo("e-enable strategic applications"))
                .statusCode(200);
    }

    @Test
    public void userTestWithUnnamedParameter() {
        int userId = 3;
        given().config(RestAssured.config().logConfig (LogConfig.logConfig ().enableLoggingOfRequestAndResponseIfValidationFails (LogDetail.ALL)))
                .when()
                .get(users + "/{userId}", userId)
                .then()
                .body("id", equalTo(userId))
                .body("name", equalTo("Clementine Bauch"))
                .body("username", equalTo("Samantha"))
                .body("email", equalTo("Nathan@yesenia.net"))
                .body("address.street", equalTo("Douglas Extension"))
                .body("address.suite", equalTo("Suite 847"))
                .body("address.city", equalTo("McKenziehaven"))
                .body("address.zipcode", equalTo("59590-4157"))
                .body("address.geo.lat", equalTo("-68.6102"))
                .body("address.geo.lng", equalTo("-47.0653"))
                .body("phone", equalTo("1-463-123-4447"))
                .body("website", equalTo("ramiro.info"))
                .body("company.name", equalTo("Romaguera-Jacobson"))
                .body("company.catchPhrase", equalTo("Face to face bifurcated interface"))
                .body("company.bs", equalTo("e-enable strategic applications"))
                .statusCode(200);
    }


    @Test
    public void mappingTest() {
        int userId = 3;
        User user = given()
                .pathParam("userId", userId)
                .when()
                .get(users + "/{userId}")
                .as(User.class);

        System.out.println(user.getId());
    }


    @Test
    public void usersWithAptTest() {
        when()
                .get(users)
                .then()
                .body("address.findAll{ it.suite.contains('Apt.')}.city", hasItems( "Lukovit" ,"South Christy", "South Elvis", "Gwenborough"));
    }


    @Test
    public void usersFromCitiesStartingWithSouthTest() {
        when()
                .get(users)
                .then()
                .body("findAll{ it.address.city.startsWith('South') }.collect{ it.id }", hasItems(4, 6));
    }


    @Test
    public void printCitiesTest() {
        String response = when().get(users).asString();
        List<String> cities = from(response).getList("address.findAll{ it.suite.contains('Apt.')}.city");

        for (String city : cities) {
            System.out.println(city);
        }
    }


    @Test
    public void postUserTest() throws IOException {
        String requestBody = new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir") + "/src/test/resources/user.json")));
        Response response =
                given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post(users)
                .then()
                .body("name", equalTo("Graham Leanne"))
                .body("username", equalTo("Graham.Leanne"))
                .body("email", equalTo("graham.leanne@emailtest.com"))
                .extract()
                .response();

        int newUserId = response.path("id");
        String newUserEmail = response.path("email");
        System.out.println(newUserId);
        System.out.println(newUserEmail);

        given()
                .pathParam("userId", newUserId)
                .when()
                .get(users + "/{userId}")
                .then()
                .body("id", equalTo(newUserId))
                .body("name", equalTo("Graham Leanne"))
                .body("username", equalTo("Graham.Leanne"))
                .body("email", equalTo("graham.leanne@emailtest.com"));

    }


    @Test
    public void responseHeaderTest() {
        when()
                .get(users)
                .then()
                .header("access-control-allow-credentials", "true");
    }


    @Test
    public void requestHeaderTest() {
        given()
                .header("test-header", "testHeaderValue")
                .cookie("PHPSessionID", "testCookieValue")
                .when()
                .get(users)
                .then()
                .log().ifValidationFails()
                .statusCode(200);
                //.cookie("test-cookie", "testCookieValue111")
                //.header("test-header", "testHeaderValue111");
    }
}
