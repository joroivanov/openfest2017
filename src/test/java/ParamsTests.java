import org.junit.Test;

import io.restassured.RestAssured;

import static io.restassured.RestAssured.*;

public class ParamsTests{
    @Test
    public void paramTest() {
        RestAssured.baseURI = "http://samples.openweathermap.org";

        String responseBody = given()
                .param("q", "Plovdiv", "BG")
                .param("appid", "b1b15e88fa797225412429c1c50c122a1")
                .when()
                .get("/data/2.5/forecast")
                .then()
                .statusCode(200)
                .and().extract().body().asString();

        System.out.println(responseBody);
    }
}
