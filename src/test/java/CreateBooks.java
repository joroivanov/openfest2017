import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public class CreateBooks {
    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 3000;
    }

    /**
     * Write a method that will register new user
     * endpoint: baseURI + /api/signup
     * request body: { "username": "fake_email", "password": "fakePass" }"
     */
    @Test
    public void registerUser() {

    }

    /**
     * Write a method that will log in a user
     * endpoint: baseURI + /api/signin
     * request body: { "username": "fake_email", "password": "fakePass" }"
     */
    @Test
    public void login() {

    }

    /**
     * Write a method that will get all books
     * endpoint: baseURI + /api/book
     * TokenName: Authorization
     * TokenValue: token from login step request
     */
    @Test
    public void getBook() {

    }
}
