import com.github.javafaker.Faker;

import java.util.Random;

public class Book {
    String isbn;
    String title;
    String author;
    String publisher;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Book() {
        Faker faker = new Faker();
        this.isbn = String.valueOf(faker.number().numberBetween(10000, 20000));
        this.title = faker.book().title();
        this.author = faker.book().author();
        this.publisher = faker.book().publisher();
    }
}
